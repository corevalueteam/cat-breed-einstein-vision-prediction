({
    onUploadImage: function (component, file, base64Data) {
        console.log("JS Controller");
        var action = component.get("c.getCatPrediction");
        action.setParams({
            fileName: file.name,
            base64: base64Data
        });
        action.setCallback(this, function (a) {
            component.set("v.spinnerWaiting", false);
            var state = a.getState();
            if (state === 'ERROR') {
                console.log(a.getError());
                alert("An error has occurred");
            } else {
                component.set("v.prediction", a.getReturnValue());
            }
        });
        component.set("v.spinnerWaiting", true);
        $A.enqueueAction(action);
    }
})