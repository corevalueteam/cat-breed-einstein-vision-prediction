/**
 * Created by mgorinshteyn on 5/21/2018.
 */

trigger AttachmentTrigger on Attachment (after insert, after update) {
    if (Trigger.isAfter) {
        CatAttachmentService.addCatAttachment(Trigger.new);
    }
}