/**
 * Created by mgorinshteyn on 5/21/2018.
 */

public class CatAttachmentService {

    public static void addCatAttachment(List<Attachment> attachments) {
        System.debug('Trigger');
        List<Cat__c> cats = [SELECT Id, AttachmentId__c FROM Cat__c];
        for (Cat__c cat : cats) {
            for (Attachment attachment : attachments) {
                if (attachment.ParentId == cat.Id) {
                    cat.AttachmentId__c = attachment.Id;
                    update cat;
                }
            }
        }
    }
}