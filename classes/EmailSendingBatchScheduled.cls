
public with sharing class EmailSendingBatchScheduled implements Schedulable {
    private final String[] emailAddresses = new String[]{
            'mgorinshteyn@corevalue.net'
    };
    public void execute(SchedulableContext context) {
        Map<String, Integer> predictionToRecords = new Map<String, Integer>();
        Integer count;
        List<Cat__c> catRecords = [SELECT Id,Cat_Breed__c FROM Cat__c WHERE CreatedDate > :System.now() - 1];
        for (Cat__c cat : catRecords) {
            count = predictionToRecords.containsKey(cat.Cat_Breed__c) ? predictionToRecords.get(cat.Cat_Breed__c) : 0;
            predictionToRecords.put(cat.Cat_Breed__c, count + 1);
        }
        List<String> predictions = new List<String>(predictionToRecords.keySet());
        String textBody = '';
        for (String prediction : predictions) {
            textBody += prediction + ':   ' + predictionToRecords.get(prediction) + '\n';
        }
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject('Cat Prediction Daily Report');
        email.setPlainTextBody(textBody);
        email.setToAddresses(emailAddresses);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                email
        });
    }
}